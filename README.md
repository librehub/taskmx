# taskmx 

Simple project task bot, built with [mxbt](https://codeberg.org/librehub/mxbt).

## Instances

| Instance                  | Prefix | Help command |
| :---                      | :---:  | :---:        |
| `@taskmx:cutefunny.art`   | `t.`   | `?help`      |

## Installation

1. Clone repo
```sh
$ git clone https://codeberg.org/librehub/taskmx
$ cd taskmx
```
2. Rename `.env.example` file to `.env` and set your credits
3. Run bot
```sh
$ python -m taskmx
```

## Support

Any contacts and crytpocurrency wallets you can find on my [profile page](https://warlock.codeberg.page).


