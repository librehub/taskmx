from mxbt import Bot, Creds, Config, Listener
import asyncio
import os

from .db import Database

bot = Bot(
    creds=Creds.from_env(
        homeserver="MATRIX_HOMESERVER",
        username="MATRIX_USERNAME",
        password="MATRIX_PASSWORD"
    ),                               
    config=Config(
        prefix='t.',
        data={'db' : Database()}
    )             
)
lr = Listener(bot)

current_dir = os.path.dirname(__file__) + "/"
for filename in os.listdir(current_dir + "modules"):
    if filename.endswith(".py"):
        lr.mount_module(f"taskmx.modules.{filename[:-3]}")

if __name__ == "__main__":
    asyncio.run(lr.start_polling())



