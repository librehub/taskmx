from dataclasses import dataclass
import sqlite3

@dataclass
class Task:
    roomId: str
    taskId: int
    author: str
    body: str
    status: str

    def get_title(self) -> str:
        title = self.body
        if len(title) > 30:
            title = title[:30] + "..."
        return title

    @classmethod
    def from_tuple(cls, data: tuple):
        if not data or len(data) == 0:
            return None
        return cls(*data)

@dataclass
class Levels:
    roomId: str
    sendLevel: int
    statusLevel: int

    @classmethod
    def from_tuple(cls, data: tuple):
        if not data or len(data) == 0:
            return None
        return cls(*data)

class Database:

    def __init__(self) -> None:
        self.base = sqlite3.connect('data.db', check_same_thread=False)
        self.cursor = self.base.cursor()

        self.make_tasks_table()
        self.make_levels_table()

    def make_tasks_table(self) -> None:
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS tasks (
            roomId TEXT,
            taskId INTEGER,
            author TEXT,
            body TEXT,
            status TEXT
        )""")
        self.base.commit()

    def make_levels_table(self) -> None:
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS levels (
            roomId TEXT,
            sendLevel INTEGER,
            statusLevel INTEGER
        )""")
        self.base.commit()

    def _insert(self, table: str, values: tuple) -> None:
        items = '?, ' * len(values)
        items = items[:-2]
        self.cursor.execute(f"INSERT INTO {table} VALUES ({items})", values)
        self.base.commit()

    def _delete(self, table: str, where: str, value: str) -> None:
        self.cursor.execute(f"DELETE FROM {table} WHERE {where} = (?)", (value,))
        self.base.commit()

    def _select_one(self, table: str, where: str, value: str | int) -> tuple:
        return self.cursor.execute(f"SELECT * FROM {table} WHERE {where} = (?)", (value,)).fetchone()

    def _select_all_where(self, table: str, where: str, value: str) -> list:
        return self.cursor.execute(f"SELECT * FROM {table} WHERE {where} = (?)", (value,)).fetchall()

    def _get_count(self, table: str) -> int:
        return len(self.cursor.execute(f"SELECT * FROM {table}").fetchall())

    def _select_all(self, table: str) -> list:
        return self.cursor.execute(f"SELECT * FROM {table}").fetchall()

    def new_task(self, roomId: str, author: str, body: str) -> int: 
        index = len(self._select_all_where('tasks', 'roomId', roomId)) + 1
        self._insert('tasks', (roomId, index, author, body, 'unviewed'))
        return index

    def get_task(self, roomId: str, taskId: int) -> Task | None:
        return Task.from_tuple(self.cursor.execute(
            f"SELECT * FROM tasks WHERE roomId = (?) AND taskId = (?)",
            (roomId, taskId)
        ).fetchone())

    def get_tasks(self, roomId: str) -> list:
        array = self._select_all_where('tasks', 'roomId', roomId)
        tasks = list()
        for raw_task in array:
            tasks.append(Task.from_tuple(raw_task))
        return tasks

    def get_tasks_category(self, roomId: str, status: str) -> list:
        array = self.cursor.execute(
            f"SELECT * FROM tasks WHERE roomId = (?) AND status = (?)",
            (roomId, status)
        ).fetchall()
        tasks = list()
        for raw_task in array:
            tasks.append(Task.from_tuple(raw_task))
        return tasks

    def set_task_status(self, roomId: str, taskId: int, status: str) -> None:
        self.cursor.execute(
            f"UPDATE tasks SET status = (?) WHERE taskId = (?) AND roomId = (?)", 
            (status, taskId, roomId)
        )
        self.base.commit()

    def get_levels(self, roomId: str) -> Levels | None:
        return Levels.from_tuple(
            self._select_one('levels', 'roomId', roomId)
        )

    def set_levels(self, roomId: str, sendLevel: int, statusLevel: int) -> None:
        self._insert('levels', (roomId, sendLevel, statusLevel))    

    def update_levels(self, roomId: str, sendLevel: int, statusLevel: int) -> None:
        self.cursor.execute(
            f"UPDATE levels SET sendLevel = (?), statusLevel = (?) WHERE roomId = (?)",
            (sendLevel, statusLevel, roomId)
        )
        self.base.commit()


