from mxbt.module import Module, command
from mxbt import Context

HELP_TEXT = """
# taskmx - Simple project task manager bot

Based on: [mxbt](https://codeberg.org/librehub/mxbt)
Maintainer: @kulebyaka:cutefunny.art

All rooms are isolated with they own tasks list. So if you send task command in another room - it will connect to another room.

## Commands:

* `?help` - Display this message
* `t.show` `{taskId}` - Show task by taskId or all room tasks if id not specified 
* `t.new` `{text}` - Add new task with specified text
* `t.status` `{taskId}` `{completed/closed/reviewed}` - Set task status by taskId
* `t.setlvl` `{sendLevel}` `{statusLevel}` - Set permission level (0...100) which need for send new tasks and change task status
* `t.showlvl` - Show permission level which need for send new tasks and change task status

> By default all members can add new tasks and only admins (with 100 power level) can set task status.
> Also only admins (100 pw) can set permission level.

### Commands example:

```
t.new       : t.new This is my simple task for team
t.status    : t.status 12 completed
t.setlvl    : t.setlvl 10 100
```

## Links:

* Support room: #librehub:cutefunny.art
* Git: https://codeberg.org/librehub/taskmx
"""

class Help(Module):

    @command(prefix="?", aliases=["help", "хелп", "h", "х", "помощь"])
    async def help(self, ctx: Context) -> None:
        await ctx.send(HELP_TEXT, use_html=True, mentions=['@kulebyaka:cutefunny.art', '#librehub:cutefunny.art']) 


