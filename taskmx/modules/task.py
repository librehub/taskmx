from mxbt.module import Module, command, event, Event
from mxbt import Context, Filter

class Tasks(Module):

    def __init__(self, bot, listener):
        super().__init__(bot, listener)
        self.db = bot.config.data['db']

    @event(event_type=Event.onMemberJoin)
    async def on_bot_join(self, room, _) -> None:
        self.db.set_levels(room.room_id, 0, 100)

    @command(aliases=['new'])
    async def new(self, ctx: Context) -> None:
        if not ctx.command: return

        levels = self.db.get_levels(ctx.room_id)
        power_level = ctx.room.power_levels.get_user_level(ctx.sender)
        if power_level < levels.sendLevel:
            await ctx.react('👎️')
            await ctx.reply("You has no permissions for task creating.")
            return

        text = ctx.command.substring
        if len(text) == 0:
            await ctx.react('👎️')
            await ctx.reply('You need to specify task text!')
            return

        taskId = self.db.new_task(ctx.room_id, ctx.sender, text)

        await ctx.react('👍️')
        await ctx.reply(f'Your task are registered with {taskId} id. Thanks for your contribution!')

    @command(aliases=['show'])
    async def show(self, ctx: Context) -> None:
        if not ctx.command: return

        taskId = ctx.command.args[0] if len(ctx.command.args) > 0 else None

        if taskId:
            if taskId.isdigit():
                task = self.db.get_task(ctx.room_id, taskId)
                if task:
                    text = f"{task.body}\n\nby {task.author} • **{task.status}** • ID: {task.taskId}"
                    await ctx.reply(f"## Task #**{taskId}**\n\n" + text, use_html=True)
                else:
                    await ctx.reply(f"There is no task with **{taskId}** id!", use_html=True)
            elif taskId in ['unviewed', 'completed', 'reviewed', 'closed']:
                category = self.db.get_tasks_category(ctx.room_id, taskId)
                if len(category) == 0:
                    await ctx.reply("There is no any tasks :(")
                    return 

                text = f"## {taskId.capitalize()}\n\n"
                for task in category:
                    title = task.get_title()
                    text += f"* {title} • by {task.author} • ID: {task.taskId}\n"

                await ctx.reply(text, use_html=True)
            else:
                await ctx.reply("Incorrect taskId or category!")
        else:
            categories = {
                'Unviewed' : self.db.get_tasks_category(ctx.room_id, 'unviewed'),
                'Reviewed' : self.db.get_tasks_category(ctx.room_id, 'reviewed'),
                'Completed' : self.db.get_tasks_category(ctx.room_id, 'completed'),
                'Closed' : self.db.get_tasks_category(ctx.room_id, 'closed'),
            }

            text = ""
            for category, tasks in categories.items():
                if len(tasks) == 0: continue
                text += f"## {category} tasks\n\n"
                for task in tasks:
                    title = task.get_title()
                    text += f"* {title} • by {task.author} • ID: {task.taskId}\n"

            if text == "":
                await ctx.reply("There is no any tasks :(")
                return

            await ctx.reply(text, use_html=True, mentions=['@kulebyaka:cutefunny.art'])

    @command(aliases=['status'])
    async def status(self, ctx: Context) -> None:
        if not ctx.command: return
        
        levels = self.db.get_levels(ctx.room_id)
        power_level = ctx.room.power_levels.get_user_level(ctx.sender)
        if power_level < levels.statusLevel:
            await ctx.react('👎️')
            await ctx.reply("You has no permissions for status update.")
            return 

        taskId = ctx.command.args[0] if len(ctx.command.args) > 0 else None
        status = ctx.command.args[1] if len(ctx.command.args) > 1 else None

        if taskId and taskId.isdigit():
            if status in ['reviewed', 'completed', 'closed']:
                self.db.set_task_status(ctx.room_id, taskId, status)
                await ctx.react('👍️')
                await ctx.reply(f"Set {status} status for task with {taskId} id")
            else:
                await ctx.react('👎️')
                await ctx.reply(
                    "Incorrect status! You can set only `reviewed`, `completed` or `closed` status.",
                    use_html=True
                )
        else:
            await ctx.react('👎️')
            await ctx.reply("Incorrect taskId!")

    @command(aliases=['setlvl'])
    @Filter.has_power_level(100)
    async def setlvl(self, ctx: Context) -> None:
        if not ctx.command: return

        sendLevel = ctx.command.args[0] if len(ctx.command.args) > 0 else None
        statusLevel = ctx.command.args[1] if len(ctx.command.args) > 1 else None

        if sendLevel and sendLevel.isdigit():
            if statusLevel and statusLevel.isdigit():
                self.db.update_levels(ctx.room_id, sendLevel, statusLevel)
                await ctx.react('👍️')
                await ctx.reply(f"Set send level to {sendLevel} and status level to {statusLevel}")
            else:
                await ctx.react('👎️')
                await ctx.reply("Incorrect status level!")
        else:
            await ctx.react('👎️')
            await ctx.reply("Incorrect send level!")

    @command(aliases=['showlvl'])
    async def showlvl(self, ctx: Context) -> None:
        if not ctx.command: return

        levels = self.db.get_levels(ctx.room_id)
        sendLevel = levels.sendLevel
        statusLevel = levels.statusLevel

        await ctx.reply(f"Send level: {sendLevel}\nStatus level: {statusLevel}")

